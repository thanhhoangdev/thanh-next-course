import { Box, Modal, Stack } from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect } from "react";
import CircularProgress from "@mui/material/CircularProgress";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "#fbceb5",
  boxShadow: 24,
  p: 4,
  display: "flex",
  justifyContent: "center",
  borderRadius: "20px",
};

export default function Home() {
  const router = useRouter();

  useEffect(() => {
    router.push("/auth/signIn");
  }, []);

  return (
    <Box>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Modal
        open={true}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Stack sx={{ color: "grey.500" }}>
            <CircularProgress />
          </Stack>
        </Box>
      </Modal>
    </Box>
  );
}
