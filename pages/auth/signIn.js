import { Button } from "@mui/material";
import { Box } from "@mui/system";
import { getProviders, signIn, useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useEffect } from "react";
import styles from "../../styles/signIn.module.css";

export default function SignIn({ providers }) {
  const { data: session } = useSession();

  const router = useRouter();

  useEffect(() => {
    if (session) {
      router.push("/posts");
    }
  }, [session]);

  return (
    <Box className={styles.main}>
      <Box className={styles.marinSupport}></Box>
      {Object.values(providers).map((provider) => (
        <Box key={provider.name} className={styles.signInForm}>
          <Box className={styles.contentForm}>
            <Box className={styles.titleSignIn}>
              Sign In to access the admin
            </Box>
            <Button variant="contained" onClick={() => signIn(provider.id)}>
              Sign in with {provider.name}
            </Button>
          </Box>
        </Box>
      ))}

      <Box className={styles.overlay}></Box>
    </Box>
  );
}

export async function getServerSideProps(context) {
  const providers = await getProviders();
  return {
    props: { providers },
  };
}
