import ThumbUpIcon from "@mui/icons-material/ThumbUp";
import ViewListIcon from "@mui/icons-material/ViewList";
import {
  Box,
  Button,
  Grid,
  MenuItem,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import Pagination from "@mui/material/Pagination";
import moment from "moment";
import Image from "next/image";
import { useEffect, useState } from "react";
import styles from "../../styles/posts.module.css";
import { useRouter } from "next/router";
import { useSession, signOut } from "next-auth/react";

export default function Posts({ posts }) {
  const [totalPage, setTotalPage] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [openPopup, setOpenPopup] = useState(false);
  const [filterByLike, setFilterByLike] = useState(0);
  const [myPosts, setMyPosts] = useState([]);

  const { data: session, status } = useSession();

  const router = useRouter();

  useEffect(() => {
    if (posts.data.length) {
      setTotalPage(Math.ceil(posts.data.length / 10));
    }
  }, [posts]);

  useEffect(() => {
    if (!session && status !== "loading") {
      router.push("/auth/signIn");
    }
  }, [status]);

  useEffect(() => {
    if (filterByLike === 1) {
      let myPosts = posts.data
        ?.slice((currentPage - 1) * 10, (currentPage - 1) * 10 + 10)
        .sort((a, b) => a.likes - b.likes);

      setMyPosts(myPosts);
    } else if (filterByLike === 2) {
      let myPosts = posts.data
        ?.slice((currentPage - 1) * 10, (currentPage - 1) * 10 + 10)
        .sort((a, b) => b.likes - a.likes);
      setMyPosts(myPosts);
    } else {
      setMyPosts(
        posts.data?.slice((currentPage - 1) * 10, (currentPage - 1) * 10 + 10)
      );
    }
  }, [filterByLike, currentPage]);

  const handleChangePage = (e, page) => {
    setCurrentPage(page);
  };

  const handleGoToDetail = (id) => {
    router.push(`posts/${id}`);
  };

  const handleSort = (event) => {
    setFilterByLike(event.target.value);
  };

  return (
    <Box className={styles.container}>
      <Box className={styles.header}>
        <Box className={styles.chameleonLogo}>
          <Image src="/logo.png" alt="chameleon Logo" width={36} height={40} />
          <span>chameleon</span>
        </Box>

        <Box className={styles.myAccount}>
          <img
            src={session?.user?.image}
            onClick={() => {
              setOpenPopup(!openPopup);
            }}
            className={styles.userAvatar}
          />

          {openPopup && (
            <Box className={styles.popupAccount}>
              <img
                src={session?.user?.image}
                className={styles.subUserAvatar}
              />
              <Box className={styles.userName}>{session?.user.name}</Box>
              <Box className={styles.emailAddress}>{session?.user.email}</Box>
              <Button
                variant="contained"
                className={styles.signOutBtn}
                onClick={() => {
                  signOut();
                }}
              >
                Sign Out
              </Button>
            </Box>
          )}
        </Box>
      </Box>

      <Box className={styles.navbar}>
        <Box className={styles.navItem}>
          <ViewListIcon />
          <span>Posts</span>
        </Box>
      </Box>

      <Box className={styles.mainContent}>
        <Select
          value={filterByLike}
          onChange={handleSort}
          className={styles.sortByLike}
        >
          <MenuItem value={0}>Filter</MenuItem>
          <MenuItem value={1}>Asc likes</MenuItem>
          <MenuItem value={2}>Desc likes</MenuItem>
        </Select>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TableContainer component={Paper}>
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow className={styles.postTitle}>
                    <TableCell>No</TableCell>
                    <TableCell>Avatar</TableCell>
                    <TableCell>Member Name</TableCell>
                    <TableCell>Image</TableCell>
                    <TableCell>Likes</TableCell>
                    <TableCell>Publish Date</TableCell>
                    <TableCell>Tags</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {posts.data?.length &&
                    myPosts?.map((post, index) => (
                      <TableRow
                        key={post.id}
                        className={styles.post}
                        onClick={() => {
                          handleGoToDetail(post.id);
                        }}
                      >
                        <TableCell>
                          {index + 1 + (currentPage - 1) * 10}
                        </TableCell>
                        <TableCell>
                          <img
                            src={post.owner.picture}
                            alt="avatar"
                            className={styles.avatar}
                          />
                        </TableCell>
                        <TableCell>{`${post.owner.title}. ${post.owner.firstName} ${post.owner.lastName}`}</TableCell>
                        <TableCell>
                          <img
                            src={post.image}
                            alt="image"
                            className={styles.image}
                          />
                        </TableCell>
                        <TableCell>
                          <Box className={styles.likes}>
                            <span>{post.likes}</span> <ThumbUpIcon />
                          </Box>
                        </TableCell>
                        <TableCell>
                          {moment(post.publishDate).format(
                            "YYYY/MM/DD, HH:mm:ss"
                          )}
                        </TableCell>
                        <TableCell>
                          <Box className={styles.tags}>
                            {post.tags.map((tag, index) => (
                              <Box className={styles.tag} key={index}>
                                {tag}
                              </Box>
                            ))}
                          </Box>
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </TableContainer>

            <Box className={styles.pagination}>
              <Pagination
                count={totalPage}
                page={currentPage}
                variant="outlined"
                shape="rounded"
                onChange={handleChangePage}
              />
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}

export async function getStaticProps() {
  const response = await fetch("https://dummyapi.io/data/v1/post", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "app-id": "62abeefeb68fac17a78f3ede",
    },
  });

  const posts = await response.json();

  return {
    props: {
      posts,
    },
  };
}
