import { Box } from "@mui/system";
import { useSession, signOut } from "next-auth/react";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Button, Container, Grid, Paper } from "@mui/material";
import Image from "next/image";
import styles from "../../styles/posts.module.css";
import postStyles from "../../styles/post.module.css";
import moment from "moment";
import ThumbUpIcon from "@mui/icons-material/ThumbUp";

export default function Post({ postDetail }) {
  const { data: session, status } = useSession();
  const [openPopup, setOpenPopup] = useState(false);

  const router = useRouter();

  useEffect(() => {
    if (!session && status !== "loading") {
      router.push("/auth/signIn");
    }
  }, [status]);

  return (
    <Box sx={{ background: "#ccc" }}>
      <Box className={styles.header}>
        <Box className={styles.chameleonLogo}>
          <Image src="/logo.png" alt="chameleon Logo" width={36} height={40} />
          <span>chameleon</span>
        </Box>
        <Box className={styles.myAccount}>
          <img
            src={session?.user?.image}
            onClick={() => {
              setOpenPopup(!openPopup);
            }}
            className={styles.userAvatar}
          />

          {openPopup && (
            <Box className={styles.popupAccount}>
              <img
                src={session?.user?.image}
                className={styles.subUserAvatar}
              />
              <Box className={styles.userName}>{session?.user.name}</Box>
              <Box className={styles.emailAddress}>{session?.user.email}</Box>
              <Button
                variant="contained"
                className={styles.signOutBtn}
                onClick={() => {
                  signOut();
                }}
              >
                Sign Out
              </Button>
            </Box>
          )}
        </Box>
      </Box>

      {/* detail */}

      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Container>
            <Button
              variant="contained"
              onClick={() => {
                router.push("/posts");
              }}
              className={postStyles.backBtn}
            >
              Back
            </Button>
            <Box
              className={postStyles.mainContent}
              onClick={() => {
                setOpenPopup(false);
              }}
            >
              <Box className={postStyles.memberInfo}>
                <img
                  src={postDetail?.owner?.picture}
                  className={postStyles.avatar}
                />

                <Box className={postStyles.memberContent}>
                  <Box
                    className={postStyles.memberContentName}
                  >{`${postDetail?.owner?.title}. ${postDetail?.owner?.firstName} ${postDetail?.owner?.lastName}`}</Box>
                  <Box>
                    {moment(postDetail?.publishDate).format(
                      "YYYY/MM/DD, HH:mm:ss"
                    )}
                  </Box>
                </Box>
              </Box>

              <Box className={postStyles.itemInfo}>
                <img
                  src={postDetail?.image}
                  className={postStyles.itemInfoImage}
                />

                <Box>
                  <Box>
                    {moment(postDetail?.publishDate).format(
                      "YYYY/MM/DD, HH:mm:ss"
                    )}
                  </Box>
                  <Box className={postStyles.itemInfoText}>
                    {postDetail?.text}
                  </Box>
                  <Box className={postStyles.tags}>
                    {postDetail.tags.map((tag, index) => (
                      <Box className={postStyles.tag} key={index}>
                        {tag}
                      </Box>
                    ))}
                  </Box>
                  <Box className={postStyles.itemInfoLikes}>
                    <span>{postDetail?.likes}</span>
                    <ThumbUpIcon />
                  </Box>
                </Box>
              </Box>
            </Box>
          </Container>
        </Grid>
      </Grid>

      <footer className={postStyles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{" "}
          <span className={postStyles.logoFooter}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </Box>
  );
}

export async function getStaticPaths() {
  const response = await fetch("https://dummyapi.io/data/v1/post", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "app-id": "62abeefeb68fac17a78f3ede",
    },
  });

  const posts = await response.json();

  const paths = posts.data.map((post) => {
    return {
      params: {
        postId: post.id,
      },
    };
  });

  return {
    paths: paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const res = await fetch(`https://dummyapi.io/data/v1/post/${params.postId}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "app-id": "62abeefeb68fac17a78f3ede",
    },
  });

  const postDetail = await res.json();

  return {
    props: {
      postDetail,
    },
  };
}
